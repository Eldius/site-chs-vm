#!/bin/bash

export HTTP_ROOT="/vagrant/target/wordpress"

IFS='/' read -a array <<< "$HTTP_ROOT"

export TMP_FOLDER=""
for index in "${!array[@]}"
do
    echo "###############################################"
    if [ "${array[index]}" != "" ];
    then
        echo "folder '$TMP_FOLDER'"
        TMP_FOLDER="$TMP_FOLDER/${array[index]}"
        if [ -f "$TMP_FOLDER" ];
        then
            echo "folder '$TMP_FOLDER' already exists"
        else
            echo "creating folder '$TMP_FOLDER'"
            mkdir $TMP_FOLDER
        fi
    fi
    echo "###############################################"
done

apt-get install nginx -y


#cp /vagrant/config-scripts/config-files/index.html $HTTP_ROOT
cat <<EOT >> $HTTP_ROOT/index.html
<html>
    <head>
        <title>Home</title>
    </head>
    <body>
        <h1>Home</h1>
    </body>
</html>

EOT

#cp /vagrant/config-scripts/config-files/nginx.conf /etc/nginx
rm /etc/nginx/nginx.conf
touch /etc/nginx/nginx.conf

cat <<EOT >> /etc/nginx/nginx.conf

# location: /etc/nginx/nginx.conf

user www-data;
worker_processes 4;
pid /var/run/nginx.pid;

events {
	worker_connections 768;
}


http {

	server {
		listen 80;
		location / {
			root              /vagrant/target/www;
			access_log        off;
			expires           -1;
			add_header        Cache-Control private;
		}

		location /api/ {
			proxy_pass http://localhost:8080/;
		}

	}

	sendfile off;
	tcp_nopush on;
	tcp_nodelay on;
	keepalive_timeout 65;
	types_hash_max_size 2048;

	include /etc/nginx/mime.types;
	default_type application/octet-stream;

	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;

	gzip on;
	gzip_disable "msie6";

	gzip_vary on;
	gzip_proxied any;
	gzip_comp_level 6;
	gzip_buffers 16 8k;
	gzip_http_version 1.1;
	gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;

	include /etc/nginx/conf.d/*.conf;
	# include /etc/nginx/sites-enabled/*;
}



EOT


service nginx restart
