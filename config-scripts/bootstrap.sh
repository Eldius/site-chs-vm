#!/bin/bash

apt-get update

apt-get upgrade -y

apt-get install -qqy debconf-utils

apt-get install -y \
  joe \
  mc \
  wget \
  curl \
  git \
  git-core \
  build-essential

/vagrant/config-scripts/install_mariadb.sh
/vagrant/config-scripts/install_nginx.sh
/vagrant/config-scripts/install_php.sh
/vagrant/config-scripts/config_wordpress.sh

echo Fim...
