#!/bin/sh

apt-get update

cat << EOF | debconf-set-selections
mysql-server-5.0 mysql-server/root_password password senha_root
mysql-server-5.0 mysql-server/root_password_again password senha_root
mysql-server-5.0 mysql-server/root_password seen true
mysql-server-5.0 mysql-server/root_password_again seen true
EOF
 
apt-get install -y mariadb-server
 
sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/my.cnf
 
service mysql restart
 
mysql -u root --password=senha_root -t <<STOP
-- This is a comment inside an sql-command-stream.
create database wordpress;
show databases;
GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpress'@'%' IDENTIFIED BY 'senha_wordpress';
\q
STOP
test $? = 0 && echo "Your batch job terminated gracefully"
