<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', 'senha_wordpress');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i9tiXCd<QC=!>  K>|D#[FLNK4-|mUu(&YXw($Aa3.4<rpKQ[z&P{?u[KS=8t&(i');
define('SECURE_AUTH_KEY',  'P q`]v@F%6,6(bqnhJ#uUcgS|-I1B4K+|UKQCSoW~M*Ju4DOR8Ir4T7+`/C+!reT');
define('LOGGED_IN_KEY',    '!0Dcz/~|9t*6HmtNhwMxhH]zh]W1R g11DRWa_t/A5!{rP|ox3FP)m^Mm`T]Zu6v');
define('NONCE_KEY',        'T~+Jny^U`b%4gV=X0vd4ps5YO$>{.Q31&w1kleJX!Kr1j49Mp@0sp8g-XO~~Z$b#');
define('AUTH_SALT',        'sTn!0&;h)+~$}6tx#PX{dD:zVmz0A(M{C++;9W%uz1@-JEh&FIX_qmOk+R8(5t4$');
define('SECURE_AUTH_SALT', 'X2KKXZjwzx>B0zSvTqX+JotHXUPN2rq% <>&Ct;(C_yH_0=x_Tw>Cv(8be{*P;}A');
define('LOGGED_IN_SALT',   '?t Qk|01Z-+!)^~p|F=ipvq{)]1CMKFy@%jw&>wUt>eqiQsl/8 34Q57iF82{APm');
define('NONCE_SALT',       '3u/Fl4B3gbB|/~c@B3W4<n] l5CY|&z#@-~Fc-lYyjfKqfU(a7b+J9>y~^5V[*N4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/*  Configuracao para nao pedir ftp */
define('FS_METHOD','direct');
