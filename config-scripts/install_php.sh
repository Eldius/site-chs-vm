#!/bin/sh

echo ##################################
echo #       UPDATING PACKAGES        #
echo ##################################

apt-get update

apt-get install -y php5-fpm php5-mysql

echo ##################################
echo #           CONFIG PHP           #
echo ##################################

cp /etc/php5/fpm/php.ini /etc/php5/fpm/php.bkp.ini
sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g' /etc/php5/fpm/php.ini

service php5-fpm restart

echo ##################################
echo #          CONFIG NGINX          #
echo ##################################

cp /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bkp

truncate -s 0 /etc/nginx/sites-available/default

rm /etc/nginx/nginx.conf

cat <<EOT >> /etc/nginx/nginx.conf

# location: /etc/nginx/nginx.conf

user www-data;
worker_processes 4;
pid /var/run/nginx.pid;

events {
	worker_connections 768;
}


http {

	server {
		listen 80;
		location / {
			root              /vagrant/target/wordpress;
			access_log        off;
			expires           -1;
			add_header        Cache-Control private;
		}

		location ~ \.php$ {
			try_files \$uri =404;
			fastcgi_split_path_info ^(.+\.php)(/.+)$;
			fastcgi_pass unix:/var/run/php5-fpm.sock;
			fastcgi_index index.php;
			fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
			include fastcgi_params;
		}


	}

	sendfile off;
	tcp_nopush on;
	tcp_nodelay on;
	keepalive_timeout 65;
	types_hash_max_size 2048;

	include /etc/nginx/mime.types;
	default_type application/octet-stream;

	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;

	gzip on;
	gzip_disable "msie6";

	gzip_vary on;
	gzip_proxied any;
	gzip_comp_level 6;
	gzip_buffers 16 8k;
	gzip_http_version 1.1;
	gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;

	include /etc/nginx/conf.d/*.conf;
	# include /etc/nginx/sites-enabled/*;
}



EOT


echo ##################################
echo #       RESTARTING NGINX         #
echo ##################################

service nginx restart



echo ##################################
echo #      CREATING TEST FILE        #
echo ##################################

cat <<EOT >> /vagrant/target/wordpress/info.php

<?php
	phpinfo();
?>

EOT

